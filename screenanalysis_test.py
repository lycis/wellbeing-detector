import pyautogui
import pytesseract
import cv2 
import numpy as np
import keras_ocr
import matplotlib.pyplot as plt
from PIL import ImageGrab
from functools import partial


def get_dominant_color(img: cv2.Mat) -> int:
    number_of_white_pix = np.sum(img == 255)
    number_of_black_pix = np.sum(img == 0)

    return number_of_white_pix > number_of_black_pix


def take_screenshot():
    ImageGrab.grab = partial(ImageGrab.grab, all_screens=True)
    screen = pyautogui.screenshot()
    screen.save("./database/screenshot.png")


def prepare_screenshot() -> tuple:
    img = cv2.imread("./database/screenshot.png")
    grayscale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh1 = cv2.threshold(grayscale, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)
    if not get_dominant_color(thresh1):
        thresh1 = cv2.bitwise_not(thresh1)
    cv2.imwrite('./database/threshold_image.jpg',thresh1)
    return (img, thresh1)


def analyse_with_keras(img: cv2.Mat):
    cv2.imwrite('./database/keras_ocr_input.jpg', img)
    images = [keras_ocr.tools.read('./database/keras_ocr_input.jpg')]
    pipeline = keras_ocr.pipeline.Pipeline()
    prediction_groups = pipeline.recognize(images)
    fig, axs = plt.subplots(nrows=len(images), ncols=1, figsize=(20, 20))
    for image, predictions in zip(images, prediction_groups):
        keras_ocr.tools.drawAnnotations(image=image, predictions=predictions, ax=axs)
    plt.savefig('./database/keras_fig.png')

    print(predictions)


def analyse_with_tesseract(img: cv2.Mat, thresholdImage: cv2.Mat):
    pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
    rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (12, 12))
    dilation = cv2.dilate(thresholdImage, rect_kernel, iterations = 3)
    cv2.imwrite('./database/dilation_image.jpg',dilation)
    contours, hierarchy = cv2.findContours(dilation, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    im2 = img.copy()
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        
        # Draw the bounding box on the text area
        rect=cv2.rectangle(im2, (x, y), (x + w, y + h), (0, 255, 0), 2)
        
        # Crop the bounding box area
        cropped = im2[y:y + h, x:x + w]
        
        cv2.imwrite('./database/rectanglebox.jpg',rect)
        
        # open the text file
        file = open("./database/text_output2.txt", "a")
        
        # Using tesseract on the cropped image area to get text
        text = pytesseract.image_to_string(cropped)
        
        # Adding the text to the file
        file.write(text)
        file.write("\n")
        
        # Closing the file
        file.close

    #print(pytesseract.image_to_string(screen))

if __name__ == "__main__":
    take_screenshot()
    (img, thresholdImage) = prepare_screenshot()
    #analyse_with_tesseract(img, thresholdImage)
    analyse_with_keras(img)
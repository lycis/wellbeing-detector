from flask import send_from_directory, Flask, current_app
from threading import Thread
from queue import Queue

# creates a Flask application
WebApp = Flask(__name__)


@WebApp.route('/')
def send_report(path):
    return current_app.static_folder('webui')

class WebUiThread(Thread):
    def __init__(self, queue: Queue, kwargs=None):
        Thread.__init__(self, args=(), kwargs=None)
        self.queue = queue

    def run(self) -> None:
        WebApp.run(debug=True)
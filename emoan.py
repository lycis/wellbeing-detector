from abc import ABC, abstractmethod
from fer import FER
from deepface import DeepFace

class AnalysisResult():
    x: int
    y: int
    width: int
    height: int

    anger: float
    disgust: float
    fear: float
    happiness: float
    sadness: float
    surprise: float
    neutral: float

    def __init__(self) -> None:
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0

        self.anger = 0
        self.disgust = 0
        self.fear = 0
        self.happiness = 0
        self.sadness = 0
        self.surprise = 0
        self.neutral = 0

    def as_dict(self) -> dict:
        return {
            "x": self.x,
            "y": self.y,
            "width": self.width,
            "height": self.height,
            "anger": self.anger,
            "fear": self.fear,
            "disgust": self.disgust,
            "happiness": self.happiness,
            "sadness": self.sadness,
            "surprise": self.surprise,
            "neutral": self.neutral 
        }
    
    @staticmethod
    def from_dict(d: dict):
        r = AnalysisResult()
        r.x = d["x"]
        r.y = d["y"]
        r.width = d["width"]
        r.height = d["height"]
        r.anger = d["anger"]
        r.disgust = d["disgust"]
        r.fear = d["fear"]
        r.happiness = d["happiness"]
        r.sadness = d["sadness"]
        r.surprise = d["surprise"]
        r.neutral = d["neutral"]
        return r


class EmotionalAnalyzer(ABC):
    @abstractmethod
    def analyze(self, imgPath) -> AnalysisResult:
        return NotImplemented

class FEREmotionalAnalyzer(EmotionalAnalyzer):
    def __init__(self) -> None:
        super().__init__()

    def analyze(self, imgPath) -> AnalysisResult:
        detector = FER()
        r = detector.detect_emotions(imgPath)

        if len(r) < 1:
            return None

        result = AnalysisResult()
        result.x = r[0]['box'][0]
        result.y = r[0]['box'][1]
        result.height = r[0]['box'][2]
        result.width = r[0]['box'][3]

        result.anger = r[0]['emotions']['angry']
        result.disgust = r[0]['emotions']['disgust']
        result.fear = r[0]['emotions']['fear']
        result.happiness = r[0]['emotions']['happy']
        result.sadness = r[0]['emotions']['sad']
        result.surprise = r[0]['emotions']['surprise']
        result.neutral = r[0]['emotions']['neutral']

        return result

class DeepfaceEmotionalAnalyzer(EmotionalAnalyzer):
    def __init__(self) -> None:
        super().__init__()

    def analyze(self, imgPath) -> AnalysisResult:
        try:
            analysis = DeepFace.analyze(img_path = imgPath, actions = ["emotion"])
        except ValueError:
            return None

        if len(analysis) < 1:
            return None

        result = AnalysisResult()
        result.x = analysis[0]['region']["x"]
        result.y = analysis[0]['region']["y"]
        result.width = analysis[0]['region']["w"]
        result.height = analysis[0]['region']["h"]
        result.anger = analysis[0]['emotion']['angry'] / 100.0
        result.disgust = analysis[0]['emotion']['disgust'] / 100.0
        result.fear = analysis[0]['emotion']['fear'] / 100.0
        result.happiness = analysis[0]['emotion']['happy'] / 100.0
        result.sadness = analysis[0]['emotion']['sad'] / 100.0
        result.surprise = analysis[0]['emotion']['surprise'] / 100.0
        result.neutral = analysis[0]['emotion']['neutral'] / 100.0

        return result


def analyze_emotions(imgPath: str, method: str = 'fer') -> AnalysisResult:
    analyzer = None
    match method.lower():
        case 'fer': 
            analyzer = FEREmotionalAnalyzer()
        case 'deepface':
            analyzer = DeepfaceEmotionalAnalyzer()
        case _:
            raise Exception("invalid emotional analysis method")
    return analyzer.analyze(imgPath)
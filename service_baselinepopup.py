import json
import tkinter as tk
from tkinter import ttk
import pika


root = tk.Tk()
rabbitChannel = None


def recordResponse(r: int):
        global rabbitChannel
        print("recorded response: {}".format(r))
        rabbitChannel.basic_publish(exchange='', routing_key='sd_emotional_response', body=json.dumps({"response": r}))
        root.withdraw()
        root.after(10000, queryStatus)    


class Application(ttk.Frame):
    def __init__(self, master: tk.Misc):
        super().__init__(master)
        self.pack()

        lbl = tk.Label(self, text="On a scale from 0 to 10: How do you currently feel?")
        lbl.grid(row=0, column=0, columnspan=11)

        btn0 = tk.Button(self, text="0", command=lambda: recordResponse(0))
        btn0.grid(row=1, column=0)
        btn1 = tk.Button(self, text="1", command=lambda: recordResponse(1))
        btn1.grid(row=1, column=1)
        btn2 = tk.Button(self, text="2", command=lambda: recordResponse(2))
        btn2.grid(row=1, column=2)
        btn3 = tk.Button(self, text="3", command=lambda: recordResponse(3))
        btn3.grid(row=1, column=3)
        btn4 = tk.Button(self, text="4", command=lambda: recordResponse(4))
        btn4.grid(row=1, column=4)
        btn5 = tk.Button(self, text="5", command=lambda: recordResponse(5))
        btn5.grid(row=1, column=5)
        btn6 = tk.Button(self, text="6", command=lambda: recordResponse(6))
        btn6.grid(row=1, column=6)
        btn7 = tk.Button(self, text="7", command=lambda: recordResponse(7))
        btn7.grid(row=1, column=7)
        btn8 = tk.Button(self, text="8", command=lambda: recordResponse(8))
        btn8.grid(row=1, column=8)        
        btn9 = tk.Button(self, text="9", command=lambda: recordResponse(9))
        btn9.grid(row=1, column=9)
        btn10= tk.Button(self, text="10", command=lambda: recordResponse(10))
        btn10.grid(row=1, column=10)


def queryStatus():
    global root
    root.deiconify()

if __name__ == "__main__":
    rabbitConnection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', credentials=pika.PlainCredentials("user", "password")))
    rabbitChannel = rabbitConnection.channel()
    rabbitChannel.queue_declare("sd_emotional_response")

    print("RabbitMQ Connection established.")
    app = Application(root)
    root.withdraw()
    root.after(10000, queryStatus)
    root.mainloop()

    rabbitConnection.close()
import datetime
import json
from matplotlib import pyplot as plt
import emoan
import pika
import csv

emotions = ['angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']
timeCharts = {} # chart for last minute tracking
currentBarChart = None # chart for current frame
emotionalResponseData = [5, 5, 5, 5, 5, 5, 5, 5, 5]
emotionalResponseChart = None
axs = []


def run(tickInterval):

    global currentBarChart
    global timeCharts
    global emotionalResponseChart
    global emotionalResponseData

    rabbitConnection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', credentials=pika.PlainCredentials("user", "password")))
    rabbitChannel = rabbitConnection.channel()
    rabbitChannel.queue_declare("sd_face_detected")
    rabbitChannel.queue_declare("sd_emotional_response")
    print("RabbitMQ Connection established.")
   
    ax = plt.subplot(3, 1, 1)
    for e in emotions:
        timesliceData = [i*0 for i in range(int(60/tickInterval))]
        h1, = plt.plot(range(len(timesliceData)), timesliceData, label=e)
        timeCharts[e] = [h1, timesliceData]
    plt.ylim(0, 1)
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.25), ncol=4, fancybox=True, shadow=True)
    axs.append(ax)

    # chart for current moment
    ax = plt.subplot(3, 1, 2)
    data = [0, 1, 0, 1, 0, 1, 0]
    currentBarChart = plt.bar(range(len(data)), data)
    ax.set_xticklabels(["x"] + emotions)
    axs.append(ax)

    # chart for emotional responses
    ax = plt.subplot(3, 1, 3)
    emotionalResponseChart, = plt.plot(range(len(emotionalResponseData)), emotionalResponseData, label="Emotional Response")
    plt.ylim(0, 10)
    axs.append(ax)    
    redraw_figure()

    
    rabbitChannel.basic_consume(queue='sd_face_detected', auto_ack=True, on_message_callback=update_chart)
    rabbitChannel.basic_consume(queue='sd_emotional_response', auto_ack=True, on_message_callback=track_emotional_response)
    rabbitChannel.start_consuming()
    rabbitConnection.close()


def track_emotional_response(ch, method, properties, body):
    global emotionalResponseData
    global emotionalResponseChart

    jsonData = json.loads(body)
    print(" [x] Emotional Response received {}".format(jsonData))

    emotionalResponseData.pop(0)
    emotionalResponseData.append(jsonData['response'])
    emotionalResponseChart.set_ydata(emotionalResponseData)
    redraw_figure()

    print(emotionalResponseData)

    with open('./database/emotional_response_datalog.csv', 'a', newline="") as f:
        csv_writer = csv.writer(f)
        csv_writer.writerow([datetime.datetime.now(), jsonData['response']])

    

      
def update_chart(ch, method, properties, body):
    global currentBarChart
    global timeCharts

    jsonData = json.loads(body)
    print(" [x] Face Received {}".format(jsonData))

    msg = emoan.AnalysisResult.from_dict(jsonData)

    # csv log
    log_face_to_csv(msg)

    # update time frame tracking
    i = 0
    for e in emotions:
        timesliceData = timeCharts[e][1]
        timesliceData.pop(0)
        d = 0
        match e:
            case 'angry':
                d = msg.anger
            case 'disgust':
                d = msg.disgust
            case 'fear':
                d = msg.fear
            case 'happy':
                d = msg.happiness
            case 'sad':
                d = msg.sadness
            case 'surprise':
                d = msg.surprise
            case 'neutral':
                d = msg.neutral
        timesliceData.append(d)
        timeCharts[e][0].set_ydata(timesliceData)
        currentBarChart[i].set_height(d)                        
        i = i + 1

    redraw_figure()

def redraw_figure():
    #plt.draw()
    #plt.pause(0.00001)
    for ax in axs:
        axs.clear()
    plt.savefig('./database/charts.png')


def log_face_to_csv(face: emoan.AnalysisResult):
    with open('./database/face_emotions_datalog.csv', 'a', newline="") as f:
        csv_writer = csv.writer(f)
        csv_writer.writerow([datetime.datetime.now(), face.anger, face.disgust, face.fear, face.happiness, face.neutral, face.sadness, face.surprise])


if __name__ == "__main__":
    run(1)
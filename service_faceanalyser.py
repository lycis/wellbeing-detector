import json
import time
import cv2
import emoan
import pika

def run(webcamPort: int, tickInterval: int):

    rabbitConnection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', credentials=pika.PlainCredentials("user", "password")))
    rabbitChannel = rabbitConnection.channel()
    rabbitChannel.queue_declare("sd_face_detected")
    print("RabbitMQ Connection established.")

    cv2.namedWindow("Emotional Analysis")
    vc = cv2.VideoCapture(webcamPort, cv2.CAP_DSHOW)
    vc.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
    vc.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)


    if vc.isOpened(): # try to get the first frame
        rval, frame = vc.read()
    else:
        rval = False

    lastResult = emoan.AnalysisResult()
    lastFaceCapture = 0

    detectionActive = False

    while rval:
        cv2.imshow("Emotional Analysis", frame)
        rval, frame = vc.read()

        if lastResult is not None:
            caption = "a={} d={} f={} h={} s={} u={} n={}".format(lastResult.anger, lastResult.disgust, lastResult.fear, lastResult.happiness, lastResult.sadness, lastResult.surprise, lastResult.neutral)
            cv2.putText(frame, caption, (lastResult.x-5, lastResult.y-5), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 255))
            cv2.rectangle(frame, (lastResult.x, lastResult.y), (lastResult.x+lastResult.width, lastResult.y+lastResult.height), (255, 0, 0), 2)

        if not detectionActive:
            cv2.putText(frame, "DETECTION PAUSED, press >> SPACE << to start", (5, 30), cv2.FONT_HERSHEY_PLAIN, 1.0, (0, 0, 255))
        else:
            cv2.putText(frame, ">> ACTIVE", (5, 30), cv2.FONT_HERSHEY_PLAIN, 1.0, (0, 255, 0))

        key = cv2.waitKey(20)
        match key:
            case 27: # exit on ESC
                break
            case 32: # start detection on SPACE
                detectionActive = not detectionActive
                if detectionActive:
                    print("Detection started.")
                else:
                    print("Detection paused.")

        if not detectionActive:
            continue

        if time.time()-lastFaceCapture >= tickInterval:
            lastFaceCapture = time.time()
            img_name = "./database/opencv_frame.png"
            cv2.imwrite(img_name, frame)
            last_img = img_name
            img = cv2.imread(last_img)
            result = emoan.analyze_emotions(img_name, 'deepface')

            if result is not None:
                print("Face detected: {}".format(result))
                lastResult = result
                rabbitChannel.basic_publish(exchange='',
                      routing_key='sd_face_detected',
                      body=json.dumps(result.as_dict()))
            else:
                print("No face detected.")

    cv2.destroyWindow("Emotional Analysis")
    vc.release()
    rabbitConnection.close()
    print("RabbitMQ Connection closed.")


def list_ports():
    """
    Test the ports and returns a tuple with the available ports and the ones that are working.
    """
    non_working_ports = []
    dev_port = 0
    working_ports = []
    available_ports = []
    while len(non_working_ports) < 6: # if there are more than 5 non working ports stop the testing. 
        camera = cv2.VideoCapture(dev_port)
        if not camera.isOpened():
            non_working_ports.append(dev_port)
            print("Port %s is not working." %dev_port)
        else:
            is_reading, img = camera.read()
            w = camera.get(3)
            h = camera.get(4)
            if is_reading:
                print("Port %s is working and reads images (%s x %s)" %(dev_port,h,w))
                working_ports.append(dev_port)
            else:
                print("Port %s for camera ( %s x %s) is present but does not read." %(dev_port,h,w))
                available_ports.append(dev_port)
        dev_port +=1
    return available_ports,working_ports,non_working_ports

if __name__ == "__main__":
    #print("Finding active video ports.")
    #ports = list_ports()
    #print("Working ports: {}".format(ports[1]))
    usedPort = int(input("Choose port: "))
    print("Port used = {}")
    run(usedPort, 1)